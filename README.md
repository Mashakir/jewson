# README #

This solution is created in visual studio 2015 with update 3 with latest .net framework 4.6.1. 

### What is this repository for? ###

* Quick summary
* Version 1.0.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
	- Set 'BranchFilePath' app setting with 'branches.json' folder location path.
	- Set the hosted web api base url in branches.js file 
* Dependencies
	- Branches.json dependency and requires to provide set it's location in web api's web config. 
* Database configuration
	- No Database configuration
* How to run tests
	- No Unit test
* Deployment instructions
	- Host the 'WebApi' published files on the IIS.
	- Host the 'Website' published files on the IIS or Directly access the Website in browser by clicking on the index.html page.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Muhammad Ahsan Shakir
* Other community or team contact